# Diploma Thesis
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

### Summary
In this repo, I maintain all the code and the text related to my diploma thesis for my degree in Civil Engineering at University of Patras.
The main focus of this thesis is the computational modelling of the flow in a gulf. However there are two more modules. One about the computation and a simple visualization of the Coriolis Effect on a parcel moving on a 2D plane, and the other a computation and a 3D visualization of the Lorenz system.
All code is written in Python3 and the GUI is made with QT5.

### Table of Contents
* [**Coriolis Effect**](Coriolis)
* [**Lorenz System**](Lorenz)
* [**Flow Modelling**](CFD)

### Authors
* **Nick Andreakos** - *Initial Work* - [nikolisan](https://github.com/nikolisan)
